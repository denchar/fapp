package com.example.denis.fapplication


import android.os.Bundle

import android.support.v7.app.AppCompatActivity;
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import android.widget.Toast

import com.google.firebase.auth.AuthResult
import com.google.android.gms.tasks.Task
import android.support.annotation.NonNull
import com.google.android.gms.tasks.OnCompleteListener
import android.R.attr.password
import android.content.Intent
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    lateinit var mAuth:FirebaseAuth
    lateinit var mAuthListener:FirebaseAuth.AuthStateListener


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mAuth = FirebaseAuth.getInstance()

     /*   mAuthListener = FirebaseAuth.AuthStateListener {

                firebaseAuth ->
            val user = firebaseAuth.currentUser
            if (user != null) {
                // User is signed in

            } else {
                // User is signed out

            }
        }
        */




        auth_btn.setOnClickListener {
            signin(email_et.text.toString(),password_et.text.toString())
        }
        reg_btn.setOnClickListener {
            registration(email_et.text.toString(),password_et.text.toString())
        }

        checkAuth()
    }

    fun checkAuth(){
        val user = mAuth.currentUser
        if (user!= null){
            var intent= Intent(this,ChatActivity::class.java)
            startActivity(intent)
        } else {

        }
    }

     fun signin(email:String, password:String) {

         mAuth
             .signInWithEmailAndPassword(email, password).addOnCompleteListener(this) { task ->
             if (task.isSuccessful) {
                 Toast.makeText(this, "Aвторизация успешна", Toast.LENGTH_SHORT).show()
                 checkAuth()
             } else
                 Toast.makeText(this, "Aвторизация провалена", Toast.LENGTH_SHORT).show()
         }

     }

    fun registration(email:String, password:String) {
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, object:OnCompleteListener<AuthResult> {

            override fun onComplete(task:Task<AuthResult>) {
                if (task.isSuccessful) {
                    Toast.makeText(applicationContext, "Регистрация успешна", Toast.LENGTH_SHORT).show()
                    checkAuth()
                }
                else
                    Toast.makeText(applicationContext, "Регистрация провалена", Toast.LENGTH_SHORT).show()
            }
        })
    }

}
