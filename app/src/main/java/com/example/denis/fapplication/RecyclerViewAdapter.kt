package com.example.denis.fapplication

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class RecyclerViewAdapter(var data: ArrayList<UserMessage>) : RecyclerView.Adapter<RecyclerViewAdapter.Holder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): Holder {
       return Holder(LayoutInflater.from(p0.context).inflate(R.layout.item_message, p0, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(p0: Holder, p1: Int) {

        p0.userName.text=data[p1].userName
        p0.userMessageTime.text=data[p1].messageTime
        p0.userMessage.text=data[p1].messageText
    }

    class Holder(view:View): RecyclerView.ViewHolder(view){

        var userName = view.findViewById<TextView>(R.id.user_tv)!!
        var userMessageTime = view.findViewById<TextView>(R.id.time_tv)!!
        var userMessage = view.findViewById<TextView>(R.id.text_message_tv)!!
    }
}