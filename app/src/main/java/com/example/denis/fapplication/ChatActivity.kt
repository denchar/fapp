package com.example.denis.fapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.LinearLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_chat.*
import java.io.Serializable



class ChatActivity : AppCompatActivity(),Serializable {

    var list=ArrayList<UserMessage>()
    var database: FirebaseDatabase=FirebaseDatabase.getInstance()
    var myRef: DatabaseReference=database.getReference("messages")
    var manager=LinearLayoutManager(this)
    var adapter=RecyclerViewAdapter(list)
    var TAG="tag"
    var  mAuth = FirebaseAuth.getInstance()


    var maxMessageLength=150


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        var user:FirebaseUser= mAuth.currentUser!!

        button_send_message.setOnClickListener {
            if (user_message_edit_text.text.toString()!=""){
                if (user_message_edit_text.text.toString().length<maxMessageLength){
                    var message = UserMessage(user_message_edit_text.text.toString().trim(),user.email.toString())
                    myRef.push().setValue(message)
                    user_message_edit_text.setText("")
                }
            }

        }

        myRef.addChildEventListener(object :ChildEventListener{
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onChildRemoved(p0: DataSnapshot) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {

                var userMessage = p0.getValue(UserMessage::class.java)

                if (userMessage != null) {
                    list.add(userMessage)
                }

                adapter.notifyDataSetChanged()
                recycler_view_message.smoothScrollToPosition(list.size)
            }
        })
        recycler_view_message.layoutManager=manager
        recycler_view_message.adapter=adapter
    }


}
