package com.example.denis.fapplication

import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

class UserMessage():Serializable {
    lateinit var userName:String
    lateinit var messageTime:String
    lateinit var messageText:String

    constructor(messageText:String, userName:String):this(){
        this.messageText=messageText
        this.userName=userName

        var simpleDateFormat = SimpleDateFormat(
            "dd-MM-yyyy (HH:mm:ss)",
            Locale.getDefault()
        )
        this.messageTime=simpleDateFormat.format(Date().time).toString()
    }
}